/* global jQuery, slgfVars */
( function ( $ ) {
	'use strict';
	$( document ).ready( function () {
		$( '.slgf-form' ).on( 'submit', function ( e ) {
			e.preventDefault();
			const $formEl = $( this );
			$formEl.block( {
				message: `<img src="${slgfVars.pluginurl}assets/images/busy.gif" />&nbsp;<span class="block-ui-message">${slgfVars.processing_message}...</span>`
			} );
			$.post( slgfVars.ajaxurl, {
				'action': 'slgf_form_submit',
				'form_data': $formEl.serialize(),
				'_wpnonce': slgfVars.nonce,
			}, function ( response ) {
				if ( response.success ) {
					$formEl.parent().html( response.data.thank_you_template );
				} else {
					$( '.slgf-feedback-message' ).html( response.data.message );
				}
			} );
		} );
	} );
} )( jQuery );

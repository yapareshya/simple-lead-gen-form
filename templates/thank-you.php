<?php
/**
 * This template is used to display the success message after form submission.
 *
 * @package     SimpleLeadGenForm
 */

?>
<div class="thank-you-wrap">
	<header>
		<h1><?php esc_html_e( 'Thank you!', 'simple-lead-gen-form' ); ?></h1>
	</header>
	<div class="thank-you-message-wrap">
		<p>
			<?php esc_html_e( 'Your submission is received and we will contact you soon.', 'simple-lead-gen-form' ); ?>
		</p>
	</div>
</div>

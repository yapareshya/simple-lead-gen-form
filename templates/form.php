<?php
/**
 * This template is used to display the Lead form.
 *
 * @package     SimpleLeadGenForm
 */

$response = wp_remote_get( 'http://worldclockapi.com/api/json/utc/now' );
global $slgf_settings;
?>
<div class="slgf-form-wrap">

	<div class="slgf-feedback-message-wrap">
		<span class="slgf-feedback-message  "></span>
	</div>

	<form action="" method="post" class="slgf-form">

		<?php if ( is_array( $response ) ) : $body = json_decode( $response['body'] ); ?>
			<input type="hidden" name="current_date_time" id="current_date_time" value="<?php echo esc_html( $body->{currentDateTime} ); ?>">
		<?php endif; ?>

		<div class="slgf-field">
			<div class="slgf-label"><label for="customer_name"><?php echo esc_html( $slgf_settings['customer_name_field_label'] ); ?></label></div>
			<div class="slgf-input"><input type="text" id="customer_name" name="customer_name" required maxlength="<?php echo esc_html( $slgf_settings['customer_name_field_maxlength'] ); ?>"></div>
		</div>
		<div class="slgf-field">
			<div class="slgf-label"><label
						for="customer_phone"><?php echo esc_html( $slgf_settings['customer_phone_field_label'] ); ?></label></div>
			<div class="slgf-input"><input type="text" id="customer_phone" name="customer_phone" required maxlength="<?php echo esc_html( $slgf_settings['customer_phone_field_maxlength'] ); ?>"></div>
		</div>
		<div class="slgf-field">
			<div class="slgf-label"><label for="customer_email"><?php echo esc_html( $slgf_settings['customer_email_field_label'] ); ?></label></div>
			<div class="slgf-input"><input type="email" id="customer_email" name="customer_email" required maxlength="<?php echo esc_html( $slgf_settings['customer_email_field_maxlength'] ); ?>"></div>
		</div>
		<div class="slgf-field">
			<div class="slgf-label"><label for="customer_budget"><?php echo esc_html( $slgf_settings['customer_budget_field_label'] ); ?></label></div>
			<div class="slgf-input"><input type="number" id="customer_budget" name="customer_budget" required maxlength="<?php echo esc_html( $slgf_settings['customer_budget_field_maxlength'] ); ?>"></div>
		</div>
		<div class="slgf-field">
			<div class="slgf-label"><label for="customer_message"><?php echo esc_html( $slgf_settings['customer_message_field_label'] ); ?></label></div>
			<div class="slgf-input"><textarea id="customer_message" name="customer_message" required ></textarea></div>
		</div>
		<div class="clear"></div>
		<div class="slgf-submit-wrap">
			<input type="submit" value="<?php esc_html_e( 'Submit', 'simple-lead-gen-form' ); ?>"/>
		</div>
	</form>
</div>

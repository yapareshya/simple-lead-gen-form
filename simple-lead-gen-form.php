<?php
/**
 * Plugin Name: Simple Lead Gen Form
 * Plugin URI:
 * Description: A Simple Lead Gen Form Plugin
 * Version:     0.1.0
 * Author:      Paresh
 * Author URI:  https://pareshradadiya.github.io/
 * Text Domain: simple-lead-gen-form
 * Domain Path: /languages
 *
 * @package SimpleLeadGenForm
 */

// Useful global constants.
define( 'SIMPLE_LEAD_GEN_FORM_VERSION', '0.1.0' );
define( 'SIMPLE_LEAD_GEN_FORM_URL', plugin_dir_url( __FILE__ ) );
define( 'SIMPLE_LEAD_GEN_FORM_PATH', plugin_dir_path( __FILE__ ) );
define( 'SIMPLE_LEAD_GEN_FORM_INC', SIMPLE_LEAD_GEN_FORM_PATH . 'includes/' );

// Include files.
require_once SIMPLE_LEAD_GEN_FORM_INC . 'functions/core.php';

// Activation/Deactivation.
register_activation_hook( __FILE__, '\SimpleLeadGenForm\Core\activate' );
register_deactivation_hook( __FILE__, '\SimpleLeadGenForm\Core\deactivate' );

// Bootstrap.
SimpleLeadGenForm\Core\setup();

// Require Composer autoloader if it exists.
if ( file_exists( SIMPLE_LEAD_GEN_FORM_PATH . '/vendor/autoload.php' ) ) {
	require_once SIMPLE_LEAD_GEN_FORM_PATH . 'vendor/autoload.php';
}

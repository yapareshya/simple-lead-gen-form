<?php
/**
 * Customer post edit class
 *
 * @package     SimpleLeadGenForm
 */

namespace SimpleLeadGenForm\Admin;

use SimpleLeadGenForm\Classes;

return new class() {

	/**
	 *  constructor.
	 */
	public function __construct() {
		$this->setup();
	}

	/**
	 * Setup actions.
	 */
	public function setup() {
		add_action( 'add_meta_boxes', [ $this, 'register_customer_detail_meta_boxes' ] );
		add_action( 'save_post', [ $this, 'save_metabox' ], 10, 2 );
		add_filter( 'manage_customer_posts_columns', [ $this, 'filter_customer_columns' ] );
		add_action( 'manage_customer_posts_custom_column', [ $this, 'action_customer_columns' ], 10, 2 );
	}

	/**
	 * Register Customer Detail metabox on customer edit screen.
	 */
	public function register_customer_detail_meta_boxes() {
		add_meta_box(
			'customer-detail-metabox',
			__( 'Customer Detail', 'simple-lead-gen-form' ),
			[
				$this,
				'customer_detail_metabox',
			],
			'customer'
		);
	}

	/**
	 * Customer detail metabox callback.
	 */
	public function customer_detail_metabox() {
		global $slgf_settings, $post;

		$customer = Classes\Customer::find( $post->ID );

		wp_nonce_field( 'save_customer_detail', 'customer_detail_nonce' );

		?>
		<table id="customer-detail-table">
			<tr>
				<td>
					<div class="slgf-label"><label
								for="customer_name"><?php echo esc_html( $slgf_settings['customer_name_field_label'] ); ?></label>
					</div>
					<div class="slgf-input">
						<input
								type="text"
								id="customer_name"
								name="customer_name"
								required
								maxlength="<?php echo esc_html( $slgf_settings['customer_name_field_maxlength'] ); ?>"
								value="<?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'customer_name' ) ) : ''; ?>"
						/>
					</div>
				</td>
				<td>
					<div class="slgf-label"><label
								for="customer_phone"><?php echo esc_html( $slgf_settings['customer_phone_field_label'] ); ?></label>
					</div>
					<div class="slgf-input">
						<input
								type="text"
								id="customer_phone"
								name="customer_phone"
								required
								maxlength="<?php echo esc_html( $slgf_settings['customer_phone_field_maxlength'] ); ?>"
								value="<?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'customer_phone' ) ) : ''; ?>"
						/>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="slgf-label"><label
								for="customer_email"><?php echo esc_html( $slgf_settings['customer_email_field_label'] ); ?></label>
					</div>
					<div class="slgf-input">
						<input
								type="email"
								id="customer_email"
								name="customer_email"
								required
								maxlength="<?php echo esc_html( $slgf_settings['customer_email_field_maxlength'] ); ?>"
								value="<?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'customer_email' ) ) : ''; ?>"
						>
					</div>
				</td>
				<td>
					<div class="slgf-label"><label
								for="customer_budget"><?php echo esc_html( $slgf_settings['customer_budget_field_label'] ); ?></label>
					</div>
					<div class="slgf-input">
						<input
								type="number"
								id="customer_budget"
								name="customer_budget"
								required
								maxlength="<?php echo esc_html( $slgf_settings['customer_budget_field_maxlength'] ); ?>"
								value="<?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'customer_budget' ) ) : ''; ?>"
						>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="slgf-label"><label
								for="customer_message"><?php echo esc_html( $slgf_settings['customer_message_field_label'] ); ?></label>
					</div>
					<div class="slgf-input">
						<textarea id="customer_message" name="customer_message" required><?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'customer_message' ) ) : ''; ?></textarea>
					</div>
				</td>
			</tr>
		</table>
		<input
				type="hidden"
				name="current_date_time"
				value="<?php echo isset( $customer ) ? esc_html( $customer->get_meta( 'current_date_time' ) ) : ''; ?>"
		/>
		<?php
	}


	/**
	 * Handles saving the meta box.
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $post Post object.
	 *
	 * @return null
	 */
	public function save_metabox( $post_id, $post ) {

		$nonce_name   = isset( $_POST['customer_detail_nonce'] ) ? sanitize_key( $_POST['customer_detail_nonce'] ) : '';
		$nonce_action = 'save_customer_detail';

		if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( wp_is_post_autosave( $post_id ) ) {
			return;
		}

		if ( wp_is_post_revision( $post_id ) ) {
			return;
		}

		$customer = Classes\Customer::find( $post_id );

		$data = [
			'customer_name'     => $_POST['customer_name'],
			'customer_phone'    => $_POST['customer_phone'],
			'customer_email'    => $_POST['customer_email'],
			'customer_budget'   => $_POST['customer_budget'],
			'customer_message'  => $_POST['customer_message'],
			'current_date_time' => $_POST['current_date_time'],
		];

		foreach ( $data as $key => $value ) {
			$customer->update_meta( $key, $value );
		}

	}

	/**
	 * Add Column on customer table.
	 *
	 * @param array $columns Customer table columns
	 *
	 * @return mixed
	 */
	public function filter_customer_columns( $columns ) {
		$columns['budget'] = esc_html__( 'Desired Budget', 'simple-lead-gen-form' );

		return $columns;
	}

	/**
	 * Manage column data on customer table.
	 *
	 * @param string $column Column slug
	 * @param int    $post_id Customer post id
	 */
	public function action_customer_columns( $column, $post_id ) {
		switch ( $column ) {
			case 'budget':
				$budget = get_post_meta( $post_id, 'customer_budget', true );
				setlocale( LC_MONETARY, 'en_US' );
				echo esc_html( money_format( '$%(#10n', $budget ) );

		}
	}

};

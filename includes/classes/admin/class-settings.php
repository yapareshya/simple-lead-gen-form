<?php
/**
 * Class for various settings for Simple Lead Gen Form Plugin
 *
 * @package     SimpleLeadGenForm
 */

namespace SimpleLeadGenForm\Admin;

return new class() {

	/**
	 *  constructor.
	 */
	public function __construct() {
		$this->setup();
	}

	/**
	 * Setup actions.
	 */
	public function setup() {
		add_action( 'admin_menu', [ $this, 'settings_menu' ] );
	}

	/**
	 * Add option page for Plugin.
	 *
	 * @return void
	 */
	public function settings_menu() {
		if ( current_user_can( 'manage_options' ) ) {
			add_options_page(
				__( 'Simple Lead Gen Form Settings', 'simple-lead-gen-form' ),
				__( 'Simple Lead Gen Form', 'simple-lead-gen-form' ),
				'manage_options',
				'simple-lead-gen-form-options',
				[ $this, 'simple_lead_gen_form_options_page' ]
			);
		}

		// Call register settings function.
		add_action( 'admin_init', [ $this, 'register_settings' ] );
	}


	/**
	 * WordPress hook for displaying plugin options page.
	 *
	 * @return void
	 * @since 4.0.0
	 */
	public function simple_lead_gen_form_options_page() {
		?>
		<div class="wrap">
			<form method="post" name="simple-lead-gen-form-settings-form" id="simple-lead-gen-form-settings-form" action="options.php">
				<?php
				settings_fields( 'simple-lead-gen-form-settings-group' );
				do_settings_sections( 'simple-lead-gen-form-options' );
				submit_button( __( 'Save Changes', 'simple-lead-gen-form' ), 'submit primary', 'slgf-submit-button', true );
				?>
			</form>
		</div>
		<?php
	}


	/**
	 * Register custom settings for Plugin.
	 *
	 * @return void
	 */
	public function register_settings() {

		$setting_fields = [
			'customer_name',
			'customer_phone',
			'customer_email',
			'customer_budget',
			'customer_message',
		];

		add_settings_section(
			'simple-lead-gen-form-settings-field-labels',
			__( 'Form Fields Label Setting', 'simple-lead-gen-form' ),
			[ $this, 'settings_section' ],
			'simple-lead-gen-form-options'
		);

		foreach ( $setting_fields as $field ) {
			register_setting( 'simple-lead-gen-form-settings-group', "slgf_{$field}_field_label", 'sanitize_text_field' );
			add_settings_field(
				"slgf_{$field}_field_label",
				/* translators: %s: field name */
				sprintf( __( '%s Field Label', 'simple-lead-gen-form' ), ucwords( str_replace( 'customer_', '', $field ) ) ),
				[ $this, 'field_label' ],
				'simple-lead-gen-form-options',
				'simple-lead-gen-form-settings-field-labels',
				[ 'field' => $field ]
			);
		}

		add_settings_section(
			'simple-lead-gen-form-settings-field-maxlength',
			__( 'Form Field Input Length Settings', 'simple-lead-gen-form' ),
			[ $this, 'settings_section' ],
			'simple-lead-gen-form-options'
		);

		$key = array_search( 'customer_message', $setting_fields, true );
		if ( false !== $key ) {
			unset( $setting_fields[ $key ] );
		}

		foreach ( $setting_fields as $field ) {
			register_setting( 'simple-lead-gen-form-settings-group', "slgf_{$field}_field_maxlength", 'sanitize_text_field' );
			add_settings_field(
				"slgf_{$field}_field_label",
				/* translators: %s: field name */
				sprintf( __( '%s Field Maximum Length', 'simple-lead-gen-form' ), ucwords( str_replace( 'customer_', '', $field ) ) ),
				[ $this, 'field_maxlength' ],
				'simple-lead-gen-form-options',
				'simple-lead-gen-form-settings-field-maxlength',
				[ 'field' => $field ]
			);
		}

		register_setting( 'simple-lead-gen-form-settings-group', 'slgf_customer_message_field_rows', 'sanitize_text_field' );
		register_setting( 'simple-lead-gen-form-settings-group', 'slgf_customer_message_field_columns', 'sanitize_text_field' );

		add_settings_field(
			'slgf_customer_message_field_rows',
			__( 'Message Field Rows', 'simple-lead-gen-form' ),
			[ $this, 'message_field_rows' ],
			'simple-lead-gen-form-options',
			'simple-lead-gen-form-settings-field-maxlength'
		);

		add_settings_field(
			'slgf_customer_message_field_columns',
			__( 'Message Field Columns', 'simple-lead-gen-form' ),
			[ $this, 'message_field_columns' ],
			'simple-lead-gen-form-options',
			'simple-lead-gen-form-settings-field-maxlength'
		);

	}

	/**
	 * Setting section callback.
	 */
	public function settings_section() {
	}

	/**
	 * Label setting fields
	 *
	 * @param array $args Extra arguments used when outputting the field.
	 */
	public function field_label( $args ) {
		global $slgf_settings;

		?>
		<input type="text" name="slgf_<?php echo esc_html( $args['field'] ); ?>_field_label" id="slgf_<?php echo esc_html( $args['field'] ); ?>_field_label" value="<?php echo esc_html( $slgf_settings[ "{$args['field']}_field_label" ] ); ?>"/>
		<?php
	}

	/**
	 * Maxlength setting fields.
	 *
	 * @param array $args Extra arguments used when outputting the field.
	 */
	public function field_maxlength( $args ) {
		global $slgf_settings;

		?>
		<input type="text" name="slgf_<?php echo esc_html( $args['field'] ); ?>_field_maxlength" id="slgf_<?php echo esc_html( $args['field'] ); ?>_field_maxlength" value="<?php echo esc_html( $slgf_settings[ "{$args['field']}_field_maxlength" ] ); ?>"/>
		<?php
	}

	/**
	 * Customer message field row.
	 */
	public function message_field_rows() {
		global $slgf_settings;

		?>
		<input type="text" name="slgf_customer_message_field_rows" id="slgf_customer_message_field_rows" value="<?php echo esc_html( $slgf_settings['customer_message_field_rows'] ); ?>"/>
		<?php
	}

	/**
	 * Customer message field column.
	 */
	public function message_field_columns() {
		global $slgf_settings;

		?>
		<input type="text" name="slgf_customer_message_field_columns" id="slgf_customer_message_field_columns" value="<?php echo esc_html( $slgf_settings['customer_message_field_columns'] ); ?>"/>
		<?php
	}

};

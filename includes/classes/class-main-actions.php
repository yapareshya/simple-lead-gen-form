<?php
/**
 * Main class.
 *
 * @package     SimpleLeadGenForm
 */

namespace SimpleLeadGenForm\Classes;

use SimpleLeadGenForm\Functions\TemplateFunctions;


return new class() {

	/**
	 *  constructor.
	 */
	public function __construct() {
		$this->setup();
	}

	/**
	 * Setup actions.
	 */
	public function setup() {
		add_action( 'init', [ $this, 'action_register_post_type' ] );
		add_shortcode( 'simple_lead_gen_form', [ $this, 'simple_lead_gen_form_template' ] );
		add_action( 'wp_ajax_slgf_form_submit', [ $this, 'process_form_submission' ] );
		add_action( 'wp_ajax_nopriv_slgf_form_submit', [ $this, 'process_form_submission' ] );
	}

	/**
	 * Register the 'customer' post type.
	 */
	public function action_register_post_type() {

		$labels = [
			'name'                  => _x( 'Customers', 'Post type general name', 'simple-lead-gen-form' ),
			'singular_name'         => _x( 'Customer', 'Post type singular name', 'simple-lead-gen-form' ),
			'menu_name'             => _x( 'Customers', 'Admin Menu text', 'simple-lead-gen-form' ),
			'name_admin_bar'        => _x( 'Customer', 'Add New on Toolbar', 'simple-lead-gen-form' ),
			'add_new'               => __( 'Add New', 'simple-lead-gen-form' ),
			'add_new_item'          => __( 'Add New Customer', 'simple-lead-gen-form' ),
			'new_item'              => __( 'New Customer', 'simple-lead-gen-form' ),
			'edit_item'             => __( 'Edit Customer', 'simple-lead-gen-form' ),
			'view_item'             => __( 'View Customer', 'simple-lead-gen-form' ),
			'all_items'             => __( 'All Customers', 'simple-lead-gen-form' ),
			'search_items'          => __( 'Search Customers', 'simple-lead-gen-form' ),
			'parent_item_colon'     => __( 'Parent Customers:', 'simple-lead-gen-form' ),
			'not_found'             => __( 'No customers found.', 'simple-lead-gen-form' ),
			'not_found_in_trash'    => __( 'No customers found in Trash.', 'simple-lead-gen-form' ),
			'insert_into_item'      => _x( 'Insert into customer', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'simple-lead-gen-form' ),
			'uploaded_to_this_item' => _x( 'Uploaded to this customer', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'simple-lead-gen-form' ),
			'filter_items_list'     => _x( 'Filter customers list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'simple-lead-gen-form' ),
			'items_list_navigation' => _x( 'Customers list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'simple-lead-gen-form' ),
			'items_list'            => _x( 'Customers list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'simple-lead-gen-form' ),
		];

		$args = [
			'labels'       => $labels,
			'public'       => false,
			'show_ui'      => true,
			'show_in_menu' => true,
			'supports'     => [ 'title', 'editor' ],
		];

		register_post_type( 'customer', $args );

		register_taxonomy(
			'customer_category',
			'customer',
			[
				'label'             => __( 'Categories', 'simple-lead-gen-form' ),
				'public'            => false,
				'show_ui'           => true,
				'show_in_menu'      => true,
				'hierarchical'      => true,
				'show_admin_column' => true,
			]
		);

		register_taxonomy(
			'customer_tag',
			'customer',
			[
				'label'        => __( 'Tags', 'simple-lead-gen-form' ),
				'public'       => false,
				'show_ui'      => true,
				'show_in_menu' => true,
			]
		);
	}

	/**
	 *  A callback for simple_lead_gen_form shortcode.
	 */
	public function simple_lead_gen_form_template() {
		ob_start();
		TemplateFunctions\get_template_part( 'form' );
		$output = ob_get_clean();

		return $output;
	}

	/**
	 * Process form submission.
	 */
	public function process_form_submission() {

		check_ajax_referer( 'slgf_form_submit' );

		if ( empty( $_POST['form_data'] ) ) {
			wp_send_json_error();
		}

		parse_str( $_POST['form_data'], $form_data );

		$customer = Customer::create(
			[
				'customer_name'     => $form_data['customer_name'],
				'customer_phone'    => $form_data['customer_phone'],
				'customer_email'    => $form_data['customer_email'],
				'customer_budget'   => $form_data['customer_budget'],
				'customer_message'  => $form_data['customer_message'],
				'current_date_time' => $form_data['current_date_time'],
			]
		);

		if ( false === $customer ) {
			wp_send_json_error(
				[
					'message' => esc_html__( 'Doh! Something went wrong, please try again.', 'simple-lead-gen-form' ),
				]
			);
		}

		ob_start();
		TemplateFunctions\get_template_part( 'thank-you' );
		$thank_you_html = ob_get_clean();

		wp_send_json_success(
			[
				'thank_you_template' => $thank_you_html,
			]
		);

	}

};

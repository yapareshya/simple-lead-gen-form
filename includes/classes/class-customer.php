<?php
/**
 * Customer Object
 *
 * @package     SimpleLeadGenForm
 */

namespace SimpleLeadGenForm\Classes;

/**
 * Customer class
 */
class Customer {

	/**
	 * Customer post id
	 *
	 * @var int
	 */
	public $id;

	/**
	 * Customer post
	 *
	 * @var object
	 */
	public $post;

	const POST_TYPE = 'customer';

	/**
	 * Customer constructor.
	 */
	public function __construct() {
	}

	/**
	 * Find customer post by id.
	 *
	 * @param int $id The customer post id.
	 *
	 * @return bool|Customer
	 */
	public static function find( $id ) {

		$post = get_post( $id );

		if ( self::POST_TYPE !== $post->post_type ) {
			return false;
		}

		$customer       = new self();
		$customer->id   = $id;
		$customer->post = $post;

		return $customer;
	}

	/**
	 * Create customer post.
	 *
	 * @param array $args Customer details
	 *
	 * @return bool|Customer
	 */
	public static function create( $args ) {

		$args = wp_parse_args(
			$args,
			[
				'customer_name'     => '',
				'customer_phone'    => '',
				'customer_email'    => '',
				'customer_budget'   => '',
				'customer_message'  => '',
				'current_date_time' => '',
			]
		);

		$customer_id = wp_insert_post(
			[
				'post_type'   => self::POST_TYPE,
				'post_status' => 'publish',
				/* translators: %s: customer name */
				'post_title'  => sprintf( __( '%s submitted lead' ), $args['customer_name'] ),
			]
		);

		if ( ! $customer_id || is_wp_error( $customer_id ) ) {
			return false;
		}

		$customer = self::find( $customer_id );

		foreach ( $args as $key => $value ) {
			$customer->update_meta( $key, $value );
		}

		return $customer;

	}

	/**
	 * Update customer post meta.
	 *
	 * @param string $key Post meta key
	 * @param mixed  $value Post meta value
	 *
	 * @return bool|int
	 */
	public function update_meta( $key, $value ) {
		return update_post_meta( $this->id, $key, $value );
	}

	/**
	 * Get customer post meta.
	 *
	 * @param string $key    Post meta key
	 * @param bool   $single Returns only the first value for the specified meta
	 *
	 * @return mixed
	 */
	public function get_meta( $key, $single = true ) {
		return get_post_meta( $this->id, $key, $single );
	}

}

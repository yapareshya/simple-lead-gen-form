<?php
/**
 * Plugin helper functions.
 *
 * @package SimpleLeadGenForm
 */

namespace SimpleLeadGenForm\Functions\Helpers;

/**
 * Get plugin settings.
 *
 * @return array
 */
function get_settings() {
	$options = get_options();

	$settings = wp_parse_args(
		$options,
		[
			'customer_name_field_label'       => 'Name',
			'customer_phone_field_label'      => 'Phone',
			'customer_email_field_label'      => 'Email',
			'customer_budget_field_label'     => 'Desired Budget',
			'customer_message_field_label'    => 'Message',
			'customer_name_field_maxlength'   => 100,
			'customer_phone_field_maxlength'  => 15,
			'customer_email_field_maxlength'  => 200,
			'customer_budget_field_maxlength' => 20,
			'customer_message_field_rows'     => 100,
			'customer_message_field_columns'  => 100,
		]
	);

	return $settings;
}

/**
 * Retrieve plugin options.
 *
 * @return array
 */
function get_options() {
	$options = [];

	$setting_fields = [ 'customer_name', 'customer_phone', 'customer_email', 'customer_budget', 'customer_message' ];

	foreach ( $setting_fields as $field ) {
		$value = get_option( "slgf_{$field}_field_label" );
		if ( ! empty( $value ) ) {
			$options[ $field . '_field_label' ] = $value;
		}
	}

	$key = array_search( 'customer_message', $setting_fields, true );
	if ( false !== $key ) {
		unset( $setting_fields[ $key ] );
	}

	foreach ( $setting_fields as $field ) {
		$value = get_option( "slgf_{$field}_field_maxlength" );
		if ( ! empty( $value ) ) {
			$options[ $field . '_field_maxlength' ] = $value;
		}
	}

	$value = get_option( 'slgf_customer_message_field_rows' );
	if ( 0 < $value ) {
		$options['customer_message_field_rows'] = $value;
	}

	$value = get_option( 'slgf_customer_message_field_columns' );
	if ( 0 < $value ) {
		$options['customer_message_field_columns'] = $value;
	}

	return $options;
}
